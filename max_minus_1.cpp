#include <iostream>
#include <iterator>
#include <algorithm>
#include <sstream>
#include <string>


using namespace std;






int main(int argc, char ** argv){


  if (argc > 1){
    //cout << "argv[1] "<< argv[1] << "\n";
  }else{
    cout << "Usage: ./max_minus_1 \"12 34 53 234 23 2\"\n";
    exit(1);
  }
  
  string input_string(argv[1]);  
  istringstream input_stream(input_string);
  
  // take all from input and put to vector of ints
  int num;
  std::vector<int> array_ints;
  while ( input_stream >> num)
    array_ints.push_back(num);
  
  //cout << "\nArray under process:\n"; 

  //copy(array_ints.begin(),
  //     array_ints.end(),
  //     ostream_iterator<int> (cout, " ")
  //);
  //cout << "\n";

  int max_in_array = max_element(array_ints.begin(), 
                                 array_ints.end()).operator*();
  //cout << "max =" << max_in_array << "\n";
  
  int count_max_minus_1 = 
      count_if(array_ints.begin(), 
               array_ints.end(),
               [max_in_array](int i){return (i == max_in_array - 1);}
          
      );

  //cout << "number of lements == max - 1 : " << count_max_minus_1 << "\n";
  
  cout << count_max_minus_1 << "\n";  
  return 0;
}
